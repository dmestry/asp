#!/bin/bash - 
#===============================================================================
#
#          FILE: net_if_setup.sh
# 
#         USAGE: ./net_if_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
confDir="/etc/sysconfig/network-scripts/"

function restartNetworkService(){
  #restarting network.service 
  echo "restarting network.service.."
  systemctl restart network.service
}

function copyNetworkInterfaceFiles(){
  echo "copying network interface configuration files"
  cd $HOME/asp
  cp -f $1 $confDir

}

function turnOnIPForwarding (){
  echo "turning on ip forwarding.."
  echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
}

function GeneratedInterfaceConfigFile (){
  
  cp -f ./ifcfg-template ifcfg-$3

  sed -i.bak -e 's/<IPAddress>/'$1'/g' ifcfg-$3
  sed -i.bak -e 's/<Prefix>/'$2'/g' ifcfg-$3 
  sed -i.bak -e 's/<Gateway>/'$4'/g' ifcfg-$3 
  sed -i.bak -e 's/<deviceName>/'$3'/g' ifcfg-$3 
  
}

function GeneratedInterfaceConfigFile_2 (){
  cp -f ./ifcfg-template ifcfg-$3
  sed -i.bak -e 's/<IPAddress>/'$1'/g' ifcfg-$3
  sed -i.bak -e 's/<Prefix>/'$2'/g' ifcfg-$3
  sed -i.bak -e 's/<deviceName>/'$3'/g' ifcfg-$3
  sed -i.bak -e 's/<DNS_1>/'$4'/g' ifcfg-$3
  sed -i.bak -e 's/<DNS_2>/'$5'/g' ifcfg-$3


}

#GeneratedInterfaceConfigFile $ipAddress $Prefix enp0s3 $Gateway
#GeneratedInterfaceConfigFile $ipAddress $Prefix enp0s8 $Gateway

#copyNetworkInterfaceFiles ifcfg-enp0s3
#copyNetworkInterfaceFiles ifcfg-enp0s8

#restartNetworkService

#turnOnIPForwarding










