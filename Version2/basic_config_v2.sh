#!/bin/sh - 
#===============================================================================
#
#          FILE: basic_config.sh
# 
#         USAGE: ./basic_config.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/12/2015 10:21
#      REVISION:  ---
#===============================================================================

set -o nounset                  # Treat unset variables as an error

declare hostname
function setHostName(){
  #set the host name, hardcoded at the moment
  hostnamectl set-hostname $1 
}

function checkHostnameStatus(){
  hostnamectl status 
}

function createUser () {
  useradd $1
  echo "$1:$2" | chpasswd
}


#set the host name, hardcoded at the moment
hostname=$1
setHostName $hostname
checkHostnameStatus 

echo "creating user1 and user2"
createUser user1 password1
createUser user2 password2



