#!/bin/bash - 
#===============================================================================
#
#          FILE: driver.sh
# 
#         USAGE: ./driver.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 11:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#source all the settings 
source master.conf


#set hostname
source basic_config_v2.sh
setHostName $hostname
checkHostnameStatus 

#disables seLinux 
source  disableSELinux.sh

#turn off NetworkManager
source manual_net_setup.sh
turnOffNetworkManager 
turnOffFirewallD

#configure network interfaces 
source net_if_setup_v2.sh
GeneratedInterfaceConfigFile $ipAddress $Prefix enp0s3 $Gateway
#GeneratedInterfaceConfigFile_2 $ipAddress $Prefix enp0s8 $Gateway
copyNetworkInterfaceFiles ifcfg-enp0s3
#copyNetworkInterfaceFiles ifcfg-enp0s8
restartNetworkService
turnOnIPForwarding



#install quagga and telnet for dynamic routing 
source quagga_setup.sh

#install dhcp
source dhcpd_setup.sh

#install and configure unbound 
source unbound_setup.sh

#install and configure wifi 
source wifi_if_setup.sh

echo "router is now wireless capable"
echo "done"




echo "done"
