ASP project README

Completed Deliverables

    - Scripts in dmestry/Scripts-Test REPO
         basic_config.sh
         basic_file_test.sh
         contributors.txt
         echo_main.sh
         func_file_test.sh
         install_vb_epel.sh
         loop_fileTest_Func.sh
         loop_fileTest_Main.sh
         printStuff.sh
         test1.txt
         whereis_ck_arg.sh


    - Project
      1. Scripted completion of all tasks
      2. Project folder Structure
          router_scripts/
          - Scripts for main service and driver scripts (ie.main_setup_driver.sh rtr_setup_driver.sh)
          config_files/
          - folder with pre-written config files that get copied into place
          email/
          - folder with scripts and ifcfg configuration file for email setup
          Version2/
          - folder with scripts for setup version 2, where settings for all scripts,services and configuration files would be taken from master configuration file (master.conf), incomplete as I needed more time to code it . 
  
                

Script Names 


```
#!bash

*             base_install.sh  - group base, git, vim, install_vp_epel.sh **Unused, Pre-requisites for Scripts to work
*             iptable_rules.sh - adds iptables rules, unused  
*             quagga_setup.sh - for installing quagga and telnet
*             basic_config.sh - sets hostname and creates users		
*             iptables_setup.sh - installs ip tables
*             rtr_setup_driver.sh - script calls setup files to install all services needed for a wireless router
*             dhcpd_setup.sh - installs and sets up dhcp		
*             main_setup_driver.sh - main setup script, use this script to install a router or email server 	
*             unbound_setup.sh - installs and sets up unbound 
*             disableSELinux.sh - disables SELinux
*             manual_net_setup.sh - turns off Network Manager and Firewalld 
*             install_vb_epel.sh - provided by instructor, installs, vim and epel 
*             net_if_setup.sh - copies network ifcfg files into place and starts/enables network service
*             dovecot_setup - sets up dovecot
*             email_setup_driver.sh - script to install email server
*             epel_setup.sh - installs epel
*             ntp_setup.sh - installs ntp
*             postfix_setup.sh - configures postfix, assumes postfix already installed
*             wifi_if_setup.sh - copies wireless config files intoplace and sets up hostapd        

       

```

Running Instructions

    Router Base: 2 adapters, one bridged to Vlan 2200, the other internal network asnet
                 git, vim, epel, base installed
    Email Base: 1 adapter internal network asnet
                 git, vim, epel, base installed
    Wireless Router
         git clone this repository to home directory (root) 
         cd  $HOME/asp/router_scripts
         bash main_setup_driver.sh router

         bash main_setup_driver.sh -h for help

    Mail Server
         git clone this repository to home directory (root) 
         cd  $HOME/asp/router_scripts
         bash main_setup_driver.sh email

         bash main_setup_driver.sh -h for help