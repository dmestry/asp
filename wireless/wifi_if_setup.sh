#!/bin/bash - 
#===============================================================================
#
#          FILE: wifi_if_setup.sh
# 
#         USAGE: ./wifi_if_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


function installHostapd () {
	echo "installing hostapd"
	yum -y install hostapd
}

function configureHostapd () {
	echo "configuring hostapd"
	cp -f $HOME/asp//wireless/ifcfg-wlp0s6u1 /etc/sysconfig/network-scripts/
	echo "restarting the network service"
	systemctl restart network.service
  cp -f $HOME/asp/config_files/hostapd.conf /etc/hostapd/

}

function startAndEnableHostapd () {
	echo "starting and enabling hostapd"
	systemctl start hostapd
	systemctl enable hostapd
}

installHostapd
configureHostapd
startAndEnableHostapd
#systemctl restart hostapd 


