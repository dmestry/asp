#!/bin/bash - 
#===============================================================================
#
#          FILE: epel_setup.sh
# 
#         USAGE: ./epel_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/12/2015 14:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
function installEpel () {
  #Install epel  
  echo "installing epel"
  yum -y install epel-release

  #Install telnet
  echo "installing telnet"
  yum -y install telnet
}

installEpel

