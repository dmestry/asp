#!/bin/bash - 
#===============================================================================
#
#          FILE: postfix_setup.sh
# 
#         USAGE: ./postfix_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/09/2015 16:53
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function restartPostfix (){
  echo "restarting postfix"
  systemctl restart postfix.service   
}

function configurePostfix (){
  echo "copying modified postfix config file, main.cf"
  cp -f $HOME/asp/config_files/main.cf /etc/postfix/
}

configurePostfix
restartPostfix




