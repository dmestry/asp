#!/bin/bash - 
#===============================================================================
#
#          FILE: dovecot_setup.sh
# 
#         USAGE: ./dovecot_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/10/2015 17:39
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


function installDovecot (){
  echo "installing dovecot"
  yum -y install dovecot
}

function startAndEnableDovecot (){
  echo "starting and enabling dovecot"
  systemctl start dovecot
  systemctl enable dovecot
}

function configureDovecot (){
  echo "editing main config file /etc/dovecot/dovecot.conf"
  cp -f $HOME/asp/config_files/dovecot.conf /etc/dovecot/
  
  echo "editing /etc/dovecot/conf.d/10-mail.conf"
  cp -f $HOME/asp/config_files/10-mail.conf /etc/dovecot/conf.d/
  
  echo "editing /etc/dovecot/conf.d/10-auth.conf"
  cp -f $HOME/asp/config_files/10-auth.conf /etc/dovecot/conf.d/
}

function restartDovecot (){
  systemctl restart dovecot 
}

installDovecot 
startAndEnableDovecot
configureDovecot

systemctl restart dovecot.service

echo "dovecot installed and configured"
