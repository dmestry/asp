#!/bin/bash - 
#===============================================================================
#
#          FILE: driver.sh
# 
#         USAGE: ./driver.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 11:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#sets hostname
source $HOME/asp/Version2/basic_config_v2.sh mail.s8.as.nasp
checkHostnameStatus

#disables seLinux 
source $HOME/asp/router_scripts/disableSELinux.sh

#turn off NetworkManager
source $HOME/asp/router_scripts/manual_net_setup.sh

#configure ifcfg files
source $HOME/asp/router_scripts/net_if_setup.sh

copyNetworkInterfaceFiles $HOME/asp/email/ifcfg-enp0s3
restartNetworkService

#install epel
source $HOME/asp/email/epel_setup.sh

#configure and restart postfix
source $HOME/asp/email/postfix_setup.sh

#install and configure dovecot
source $HOME/asp/email/dovecot_setup.sh

#intsall ntp
source $HOME/asp/email/ntp_setup.sh

echo "done, rebooting machine"
reboot

