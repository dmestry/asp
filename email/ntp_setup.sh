#!/bin/bash - 
#===============================================================================
#
#          FILE: ntp_setup.sh
# 
#         USAGE: ./ntp_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/10/2015 17:27
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function installNtp (){
  echo "installing ntp"
  yum -y install ntp 
}
function startNtp (){
  echo "starting and enabling the ntpd"
  systemctl start ntpd
  systemctl enable ntpd
}


function syncSystemClock () {
  echo "synchronizing system clock"
  timedatectl set-ntp yes
}

installNtp
startNtp
syncSystemClock

