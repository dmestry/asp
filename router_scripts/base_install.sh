#!/bin/bash - 
#===============================================================================
#
#          FILE: base_install.sh
# 
#         USAGE: ./base_install.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/19/2015 14:52
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function base_install {
  yum -y -v group install base
  yum -y install git
  yum -y install vim
  
  bash ./install_vb_epel.sh
}

base_install

