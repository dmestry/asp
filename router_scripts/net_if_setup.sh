#!/bin/bash - 
#===============================================================================
#
#          FILE: net_if_setup.sh
# 
#         USAGE: ./net_if_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
confDir="/etc/sysconfig/network-scripts/"

function restartNetworkService(){
  #restarting network.service 
  echo "restarting network.service.."
  systemctl restart network.service
}

function copyNetworkInterfaceFiles(){
  echo "copying network interface configuration files"
  cd $HOME/asp/config_files/
  cp -f $1 $confDir

}

function turnOnIPForwarding (){
  echo "turning on ip forwarding.."
  echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
}

#copyNetworkInterfaceFiles ifcfg-enp0s3
#copyNetworkInterfaceFiles ifcfg-enp0s8

#restartNetworkService

#turnOnIPForwarding

echo "done, reboot, check ip forwarding, then install quagga and telnet"








