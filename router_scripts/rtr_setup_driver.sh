#!/bin/bash - 
#===============================================================================
#
#          FILE: driver.sh
# 
#         USAGE: ./driver.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 11:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error



#sets hostname
source $HOME/asp/router_scripts/basic_config.sh 

#disables seLinux 
source  $HOME/asp/router_scripts/disableSELinux.sh

#turn off NetworkManager
source $HOME/asp/router_scripts/manual_net_setup.sh

#configure ifcfg files
source $HOME/asp/router_scripts/net_if_setup.sh

copyNetworkInterfaceFiles ifcfg-enp0s3
copyNetworkInterfaceFiles ifcfg-enp0s8

turnOnIPForwarding

#install quagga and telnet for dynamic routing 
source $HOME/asp/router_scripts/quagga_setup.sh

#install dhcp
source $HOME/asp/router_scripts/dhcpd_setup.sh

#install and configure unbound 
source $HOME/asp/router_scripts/unbound_setup.sh

#install and configure wifi 
source $HOME/asp/wireless/wifi_if_setup.sh


echo "restarting network"
systemctl restart network

echo "restarting rip"
systemctl restart ripd

echo "restarting dhcp"
systemctl restart dhcpd

echo "restarting hostapd"
systemctl restart hostapd

echo "restarting unbound"
systemctl restart unbound


echo "router is now wireless capable"
echo "done"
