#!/bin/bash - 
#===============================================================================
#
#          FILE: main_setup_driver.sh
# 
#         USAGE: ./main_setup_driver.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 06/20/2015 14:07
#      REVISION:  ---
#===============================================================================

declare vmType

vmType=$1

function Usage () {
  echo "Usage:"
  echo "  main_setup_driver.sh router --> for router setup"
  echo "  main_setup_driver.sh email --> for email setup"
  echo "  main_setup_driver.sh -h --> for this help message"
}

function getMode () {
   if [[ "$1" == "" ]]; then
     echo "Expecting an argument, try main_setup_driver.sh -h for help "
   elif [[ "$1" == "router" ]]; then
     echo  "setting up router"
     source $HOME/asp/router_scripts/rtr_setup_driver.sh
   elif [[ "$1" == "email" ]]; then
     echo "setting up email server"
     source $HOME/asp/email/email_setup_driver.sh
   elif [[ "$1" == "-h" ]]; then
     Usage
   else 
     Usage
   fi 
}


getMode $vmType

