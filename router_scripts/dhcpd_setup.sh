#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcpd_setup.sh
# 
#         USAGE: ./dhcpd_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function installDhcp (){
  echo "installing dhcp"
  yum -y install dhcp
}
function startDhcp (){
  echo "starting and enabling the dhcpd"
  systemctl start dhcpd.service 
  systemctl enable dhcpd.service
}

function copyDhcpConfig (){
  echo "modify the /etc/dhcp/dhcpd.conf file"
  cp -f $HOME/asp/config_files/dhcpd.conf /etc/dhcp/
}

installDhcp
copyDhcpConfig
startDhcp


