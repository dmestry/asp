#!/bin/bash - 
#===============================================================================
#
#          FILE: quagga_setup.sh
# 
#         USAGE: ./quagga_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:56
#      REVISION:  ---
#===============================================================================

set -o nounset       # Treat unset variables as an error

function installQuaggaAndTelnet (){
  #install package quagga, zebra daemon, ripd daemon
  echo "instalilng quagga..."
  yum -y install quagga

  echo "installing telnet..."
  yum -y install telnet
  
  echo "changing ownership of /etc/quagga directory..."
  chown -R quagga /etc/quagga/
}

function configureRipd(){ 
  echo "configuring ripd.conf,aka copying the preconfigured file into place"
  cp -f $HOME/asp/config_files/ripd.conf /etc/quagga/

}


function configureZebra(){
  echo "configuring zebra.conf,aka copying the preconfigured file into place"
  cp -f $HOME/asp/config_files/zebra.conf /etc/quagga/ 
 } 


function startZebra (){
  echo "starting and enabling zebra"
  systemctl start zebra
  systemctl enable zebra
}

function startRipd () {
  echo "starting and enabling ripd"
  systemctl start ripd
  systemctl enable ripd 
}


installQuaggaAndTelnet
configureRipd
configureZebra
startRipd
startZebra

echo "done, zebra and ripd services installed"

