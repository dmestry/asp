#!/bin/bash - 
#===============================================================================
#
#          FILE: manual_net_setup.sh
# 
#         USAGE: ./manual_net_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:11
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function turnOffNetworkManager (){
  echo "stopping and disabling NetworkManager"
  systemctl stop NetworkManager.service
  systemctl disable NetworkManager.service

}

function turnOffFirewallD(){
  echo "stopping and disable firewalld service"
  systemctl stop firewalld.service
  systemctl disable firewalld.service
}

turnOffNetworkManager 
turnOffFirewallD

