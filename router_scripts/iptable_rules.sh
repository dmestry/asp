#flushing of all of the current forwarding and nat rules

iptables -t filter --flush
iptables -t nat --flush

#set the default policy of your firewall to drop packets as they arrive

iptables -t filter -P INPUT DROP

#set the default policy to allow all packets to leave or be forwarded

iptables -t filter -P OUTPUT ACCEPT
iptables -t filter -P FORWARD ACCEPT

#allow traffic into the router from the student network and on the loopback adapter

iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A INPUT -i br0 -j ACCEPT

#allowing ssh traffic into your the enp0s3 interface of your CentOS Network VM

iptables -t filter -A INPUT -i enp0s3 -p tcp --dport 22 -j ACCEPT

#allow all traffic that is part of an existing tcp or udp connection into the router

iptables -t filter -I INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#performing network address translation on traffic leaving the enp0s3 interface of your CentOS Network VM that originated in your student network.

iptables -t nat -A POSTROUTING -o enp0s3 -s 192.168.254.0/24 -j MASQUERADE

#performing port forwarding for traffic arriving on the enp0s3 interface destined destination port 50001 and forwarding it to port 50001 on the Windows 8.1 VM.

iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 50001 -j DNAT --to-destination 192.168.254.101

#Saving the iptables rules:

sudo service iptables save
