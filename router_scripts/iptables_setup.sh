#!/bin/bash - 
#===============================================================================
#
#          FILE: iptables.setup.sh
# 
#         USAGE: ./iptables.setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo "stopping and disabling zebra and ripd"
systemctl stop ripd.service
systemctl stop zebra.service
systemctl disable ripd.service
systemctl disable zebra.service

echo "installing iptables"
yum -y install iptables 
yum -y install iptables-services
yum -y install iptables-utils

echo "starting and enabling iptables service"
systemctl enable iptables.service
systemctl start iptables.service 

#run iptables_rules.sh to apply custom rules
source iptable_rules.sh
