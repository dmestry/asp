#!/bin/bash - 
#===============================================================================
#
#          FILE: unbound_setup.sh
# 
#         USAGE: ./unbound_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/19/2015 09:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function installUnbound (){
  echo "installing unbound .."
  yum -y install unbound
}

function configureUnbound (){
  echo "configuring unbound .. "
  cp -f $HOME/asp/config_files/unbound.conf /etc/unbound/
  cp -f $HOME/asp/config_files/resolv.conf /etc/
}

function startUnbound (){
  echo "starting and enabling unbound .." 
  systemctl start unbound
  systemctl enable unbound
}

installUnbound
configureUnbound 
startUnbound 

