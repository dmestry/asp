#!/bin/bash - 
#===============================================================================
#
#          FILE: disableSELinux.sh
# 
#         USAGE: ./disableSELinux.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/14/2015 15:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


function disableSELinux (){
  cp -f /root/asp/config_files/config /etc/selinux/
}
cd $HOME/asp

